export ORACLE_BASE=/oracle
export ORACLE_HOME=/oracle/11.2.0
export PATH=$PATH:$ORACLE_HOME/bin:.
export ORACLE_SID=svfe
export LD_LIBRARY_PATH=$ORACLE_HOME/lib

export ORACLE_TERM=xterm
export NLS_LANG=AMERICAN_AMERICA.CL8MSWIN1251
export ORA_NLS33=$ORACLE_HOME/ocommon/nls/admin/data

export PS1='\u@\h:\w[$ORACLE_SID] >'
