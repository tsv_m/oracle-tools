/*
-- To run the test, just run the code. 
-- Then you can: exec DBMS_SCHEDULER.run_job (job_name => 'TEST_UTL_FILE',use_current_session => FALSE);
-- Then you can: exec DBMS_SCHEDULER.run_job (job_name => 'TEST_UTL_FILE',use_current_session => TRUE);
-- 
*/
begin
    Sys.Dbms_Scheduler.Create_Job(
      Job_Name            => 'TEST_UTL_FILE',
      Job_Type            => 'PLSQL_BLOCK',
      Job_Action          => '
declare
  v_Filename      VARCHAR2(255)       := ''write_access.log'';
  v_Location      VARCHAR2(255)       := ''/home/smartcg/output/cardgen/outfiles'';
  --
  PROCEDURE Put_Lines(p_Msg IN VARCHAR2) IS
    v_Msg VARCHAR2(32767) := p_Msg;
    v_255 VARCHAR2(255);
    v_Eol NUMBER;
  BEGIN
    IF LENGTH(v_Msg) > 255
      THEN
        WHILE v_Msg IS not null LOOP
          v_255 := SUBSTR(v_Msg, 1, 255);
          v_Eol := INSTR(v_255, '' '', -1);
          IF v_Eol = 0
            THEN v_Eol := 255;
            ELSE v_255 := SUBSTR(v_255, 1, v_Eol);
          END IF;
          DBMS_OUTPUT.PUT_LINE(v_255);
          v_Msg := SUBSTR(v_Msg, v_Eol + 1);
        END LOOP;
      ELSE DBMS_OUTPUT.PUT_LINE(v_Msg);
    END IF;
  END Put_Lines;
  --
  PROCEDURE Test_File_Write_Access
    ( p_Location  IN VARCHAR2
    , p_Filename  IN VARCHAR2
    )
  IS
    TYPE t_File_Content IS TABLE OF VARCHAR2(32767) INDEX BY BINARY_INTEGER;
    c_Max_Linesize  CONSTANT  PLS_INTEGER := 32767;
    v_Blocksize     NUMBER;
    v_Content       t_File_Content;
    v_Existence     BOOLEAN;
    v_File_Handle   UTL_FILE.FILE_TYPE;
    v_File_Pos      NUMBER;
    v_Filesize      NUMBER;
    v_i             BINARY_INTEGER;
  BEGIN
    v_File_Handle := UTL_FILE.FOPEN( location     => p_Location
                                   , filename     => p_Filename
                                   , open_mode    => ''w''
                                   , max_linesize => c_Max_Linesize );
    UTL_FILE.PUT_LINE(v_File_Handle, ''Access test passed'');
    UTL_FILE.FCLOSE(v_File_Handle);
  EXCEPTION
    WHEN UTL_FILE.INVALID_PATH        THEN
      Put_Lines(''File location or name was invalid'');
    WHEN UTL_FILE.INVALID_MODE        THEN
      Put_Lines(''The open_mode string was invalid'');
    WHEN UTL_FILE.INVALID_OPERATION   THEN
      Put_Lines(''File could not be opened as requested'');
    WHEN UTL_FILE.INVALID_MAXLINESIZE THEN
      Put_Lines(''Specified max_linesize is too large or too small'');
    WHEN OTHERS                       THEN
      Put_Lines(''Error: ''||SQLERRM);
      IF UTL_FILE.IS_OPEN(v_File_Handle) THEN
        UTL_FILE.FCLOSE(v_File_Handle);
      END IF;
  END;
  --
begin
  Test_File_Write_Access(v_Location, v_Filename);
  Put_Lines(''Test completed, please check file ''||v_Location||''/''||v_Filename);
end;
',
      Number_Of_Arguments => 0,
      Start_Date          => SYSTIMESTAMP,
      Repeat_Interval     => NULL,
      End_Date            => NULL,
      Job_Class           => 'DEFAULT_JOB_CLASS',
      Enabled             => FALSE,
      Auto_Drop           => FALSE,
      Comments            => 'test utl_file access'
    );
end;
/