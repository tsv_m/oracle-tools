set lines 300
set pages 500
select 
    owner,
    object_type,
    object_name
from
    all_objects
where
    status<>'VALID'
;
