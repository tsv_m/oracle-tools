#!/bin/bash
# "step by step suite"
# (c) dvavaev

#MAIN_CONFIG='main.conf'

function init() {
	echo
}

function set_default_constants() {
	export AS_STEPS_DIR='STEPS'
}

function run() {
	if [ -z "${MAIN_CONFIG}" ]; then
		#default logic
		echo 'Starting'
		#setting default constants
		set_default_constants
		#making a list of tasks
		export AS_STEPS_LIST=`ls -1 $AS_STEPS_DIR | sort`
		echo ${AS_STEPS_DIR}
		for step in ${AS_STEPS_LIST}; do
			bash ${step}
		done
		
	fi
}

function usage() {
	if [ -e lib/usage.txt ]; then
		cat lib/usage.txt >&2
	else
		echo 'File lib/usage.txt is empty. Ask developer, how to use this...' >&2
	fi
}

run